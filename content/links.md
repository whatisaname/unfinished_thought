---
title: "Tools and resources"
date: 2018-02-13
---

### This site
* [Hugo](https://gohugo.io/) - for the website generation
* [Call me Sam](https://github.com/vickylai/hugo-theme-sam) - for this awesome theme
* [Gitlab](https://gitlab.com) - for the hosting
