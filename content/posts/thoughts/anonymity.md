---
title: "Anonymity on the web"
date: 2018-06-15
showDate: true
draft: false
tags: ["internet"]
---

> Give a man a mask and he'll speak the truth.


Anonymity makes us feel safe and enables us to speak our mind as we know that no actual consequesnces will come to us. This also makes it easier for people who doesn't feel well in one way or another to lash out at random people on the web for no actual reason but a conjured felling of self sartesfaction.


The apperent problem that harrasing people have become easy has been tried to be solved by removing anonymity. But this only makes people that found comfort in anonymity and didn't harass others to stop coresponding only leaving the crazies and the ones not understanding what leaving a way open for others to trace things back to them, could mean.


So what is the problem if it isn't anonymity? Well, in real life we tend to ignore or stear clear of people we can't handle or tolerate. But we do this after realizing that this is the case. So something like an ignore function and a block function would probably be on the right track. But since the platforms differ, real life and the digital life, some differences in implementation might be needed.
