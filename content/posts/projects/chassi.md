---
title: "Chassi prototype - Testing some things"
date: 2018-07-27
showDate: true
draft: false
tags: []
---
# The plan

I have used small and compact mini-ITX chassis for some time and wanted to switch back to a more normal ATX formfactor with sound proofening. I got my hands on a used fractal design r2 which I used for some time. But it had some kinks, missed a leg and fast still to noisy. So I picked up my paused thoughts of building my own chassi.

The main deciding principel behind the chassis design is the fact that heat rises. Also, as it turns out, water pumps are not silent but fans are pretty damn close to. With this in mind, and the fact that I was going to build it out of wood and put wheels on it, I began to make drawings.

Since heat rises I wanted to draw in air from the bottom and out through the top. At the same time I didn't want to let dust in from the floor nor the air. I decided on a ox design with air being drawn in from the bottom, protected from the floor with a bottom plate and then pushed up through the chassi throught the heatsinks and out in the top. At the top I had a top plate with big open gaps between the side panels and the top for the air to flow out trough. This made for very low air friction in the chassi, and with 2 very silent 140 mm in the bottom and the chassi made of around 2 cm thick wood board the chassi turned out to be very quiet.

# Building it (v. 1.0)

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170627_175132.jpg "Testing motherboard plate")
Testing motherboard plate with old motherboard

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170627_175139.jpg "Testing motherboard plate")
Testing motherboard plate with old motherboard pic2

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170701_110742.jpg "Glueing togheter the base chassi")
Glueing together the base chassi with the longest clamps I could find

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170701_110751.jpg "Glueing togheter the base chassi")
Glueing together the base chassi with the longest clamps I could find pic2

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170701_130422.jpg "The base chassi done - backside")
Backside

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170701_130436.jpg "The base chassi done - front")
Front with test mobo installed

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170712_173303.jpg "Backside + wheels + hinges")
Backside with wheels and hinges attached

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20170712_173659.jpg "Backside - lids for PCI slots")
Backside with lids for the PCI slots. The lids are made of soft plastic foam carpet

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20171221_175321.jpg "System assembled")
It works!

# Refining it (v. 1.1)

The riser cabel I used to connect the GPU to the motherboard miraculesly broke which forced me to act on the shortcommings of the first iteration of my chassi design. The placing of the fan filters was very bad and resulted in the fan placing also becomming bad.

With the new refinments the GPU is going to be placable both standing and uppside down. The second option making it possible to use a shorter riser cable to connect it with my current mini-ITX motherboard. The other changes in the introduction of rubberbands as fastening for both fans, fan filters and harddrives.This makes it possible to move the fan filters to the underside of the intake plate while the fans can come closer to the bottom of the chassi and make more room for the GPU.

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_145755.jpg "Adjustable GPU support")
Adjustable GPU support

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_160743.jpg "Chassi filter fastening - first side")
First side of new chassi fan filter fastening with rubberbands

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_161331.jpg "Chassi filter fastening - other side")
Other side of new chassi fan filter fastening and new fan fastening, also rubberbands

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_161349.jpg "Filters mounted")
Filters mounted with new fastenings

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_161419.jpg "Overview shot")
Overview shot of new fastenings with parts mounted sans GPU support

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_161432.jpg "GPU fastening made adjustable")
GPU fastening made adjustable

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180713_165645.jpg "Overview shot")
Overview shot of new fastenings with parts mounted with GPU support

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180727_202056.jpg "GPU fastening WITH SCREW!")
Holes and screw for upside down mounting of GPU.

![](https://whatisaname.gitlab.io/unfinished_thought/chassi_pics/20180726_154805.jpg "Harddrive fastening")
Harddrive fastening (AKA More rubberbands!).

# Future Plans

- Semi-passive GPU like the ASUS Strix series
- Semi-passive or passive PSU

## Future Future Plans

- Make a 2.0 chassi